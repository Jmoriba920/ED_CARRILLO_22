package EntornosDesarollo.Cuenta_Tarjeta_Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.hamcrest.number.OrderingComparison.lessThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CreditoTest {

	private SimpleDateFormat formato;
	private Date fechaPrueba;

	@BeforeEach
	public void setUp() throws Exception {
		formato = new SimpleDateFormat("dd/MM/yyyy");
		fechaPrueba = formato.parse("30/06/2022");
	}

	@Test
	@DisplayName("Test Retirar menos de 3")
	public void testRetirar1() throws Exception {
		Tarjeta tarjeta = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		tarjeta.retirar(2);

		assertThat(tarjeta.getSaldo(), equalTo(3.0));
	}

	@Test
	@DisplayName("Test Retirar más del saldo")
	public void testRetirar2() throws Exception {
		Tarjeta tarjeta = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		Exception exception = assertThrows(Exception.class, () -> tarjeta.retirar(100000));

		assertFalse(exception.getMessage().contains("Saldo Insuficiente"));
	}

	@Test
	@DisplayName("Test Retirar saldo disponible")
	public void testRetirar3() throws Exception {
		Tarjeta tarjeta = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		tarjeta.retirar(900);

		assertThat(tarjeta.getSaldo(), lessThan(1000.0));
		assertThat(tarjeta.getSaldo(), equalTo(45.0));
	}

	@Test
	@DisplayName("Test Ingresar")
	public void testIngresar() throws Exception {
		Tarjeta tarjeta = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		Cuenta cuenta = new Cuenta("1", "Usuario Genérico");
		double valorAIngresar = 120.0;
		double saldoOriginal = tarjeta.getSaldo();
		tarjeta.mCuentaAsociada = cuenta;

		tarjeta.ingresar(valorAIngresar);

		assertThat(valorAIngresar, greaterThanOrEqualTo(saldoOriginal));
	}

	@Test
	@DisplayName("Test Pago Establecimiento")
	public void testPagoEstablecimiento() throws Exception {
		Tarjeta tarjeta = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		tarjeta.pagoEnEstablecimiento("Big Corner", 20.0);

		assertThat(tarjeta.getSaldo(), lessThan(1000.0));
		assertThat(tarjeta.getSaldo(), equalTo(20.0));
	}

	@Test
	@DisplayName("Test Get Saldo")
	public void testGetSaldo() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyy");
		Date fechaPrueba = formato.parse("30/06/2022");

		Tarjeta t1 = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		Cuenta cuenta1 = new Cuenta("1", "Usuario Genérico");

		t1.mCuentaAsociada = cuenta1;
		t1.ingresar(20);

		assertEquals(20, t1.getSaldo());

	}

	@Test
	@DisplayName("Test Liquidar")
	public void testLiquidar() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyy");
		Date fechaPrueba = formato.parse("30/06/2022");

		Credito t1 = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		Cuenta cuenta1 = new Cuenta("1", "Usuario Genérico");

		t1.mCuentaAsociada = cuenta1;

		double cantidadAIngresarTarjeta = 200;
		double cantidadIngresadaCuenta = 700;
		t1.ingresar(cantidadAIngresarTarjeta);
		cuenta1.ingresar(cantidadIngresadaCuenta);

		System.out.println(t1.getSaldo());

		t1.liquidar(7, 2022);

		assertThat(cantidadAIngresarTarjeta + cantidadIngresadaCuenta, equalTo(cuenta1.getSaldo()));

	}

	@Test
	@DisplayName("Test Liquidar 2")
	public void testLiquidar2() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyy");
		Date fechaPrueba = formato.parse("30/06/2022");

		Credito t2 = new Credito("1", "Usuario Genérico", fechaPrueba, 1000);
		Cuenta cuenta2 = new Cuenta("1", "Usuario Genérico");

		t2.mCuentaAsociada = cuenta2;

		t2.ingresar(100);

		Movimiento mov = (Movimiento) t2.mMovimientos.firstElement();
		mov.setFecha(fechaPrueba);

		t2.liquidar(6, 2022);
	}

}
