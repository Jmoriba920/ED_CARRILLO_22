package EntornosDesarollo.Cuenta_Tarjeta_Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CuentaTest {

	@Test
	@DisplayName("Test ingreso con concepto")
	public void testIngresoConConcepto() throws Exception {
		Cuenta cuenta1 = new Cuenta("0001", "Juan Pérez");
		cuenta1.ingresar("Salario", 2000);

		assertThat(cuenta1.getSaldo(), equalTo(2000.0));

	}

	@Test
	@DisplayName("Test ingreso sin concepto")
	public void testIngreso() throws Exception {
		Cuenta cuenta1 = new Cuenta("0002", "María López");
		cuenta1.ingresar(3000);

		assertThat(cuenta1.getSaldo(), lessThan(3001.0));

	}

	@Test
	@DisplayName("Test retiro con concepto")
	public void testRetiroConConcepto() throws Exception {
		Cuenta cuenta1 = new Cuenta("0003", "Pedro Gómez");
		cuenta1.ingresar(1000);
		cuenta1.retirar("Compra en supermercado", 500);

		Movimiento m1 = new Movimiento();

		m1 = (Movimiento) cuenta1.mMovimientos.lastElement();

		boolean encontrado = false;

		if (cuenta1.mMovimientos.lastElement().equals(m1)) {
			encontrado = true;
		}

		assertTrue(encontrado);

	}

	@Test
	@DisplayName("Test Ingresar Negativo")
	public void testIngresarNegativo() throws Exception {
		Cuenta cuenta1 = new Cuenta("0002", "María López");
		try {
			cuenta1.ingresar(-100);
			fail("Se esperaba una excepción");
		} catch (Exception e) {
			assertEquals("No se puede ingresar una cantidad negativa", e.getMessage());
		}
	}
	
	@Test
	@DisplayName("Test Ingresar Cero")
	public void testIngresarCero() throws Exception {
	    Cuenta cuenta1 = new Cuenta("0002", "María López");
	    try {
	        cuenta1.ingresar(0);
	        fail("Se esperaba una excepción");
	    } catch (Exception e) {
	        assertEquals("No se puede ingresar una cantidad negativa", e.getMessage());
	    }
	}
	
	@Test
	@DisplayName("Test Retirar Negativo")
	public void testRetirarNegativo() throws Exception {
		Cuenta cuenta1 = new Cuenta("0002", "María López");
		try {
			cuenta1.retirar(-100);
			fail("Se esperaba una excepción");
		} catch (Exception e) {
			assertEquals("No se puede retirar una cantidad negativa", e.getMessage());
		}
	}
	
	@Test
	@DisplayName("Test Retirar Cerp")
	public void testRetirarCero() throws Exception {
		Cuenta cuenta1 = new Cuenta("0002", "María López");
		try {
			cuenta1.retirar(0);
			fail("Se esperaba una excepción");
		} catch (Exception e) {
			assertEquals("No se puede retirar una cantidad negativa", e.getMessage());
		}
	}
	
	@Test
	@DisplayName("Test Ingresar Negativo")
	public void testIngresarNegativo1() {
	    Cuenta cuenta1 = new Cuenta("0002", "María López");
	    try {
	        cuenta1.ingresar("Ingreso en efectivo", -100);
	        fail("Se esperaba una excepción");
	    } catch (Exception e) {
	        assertEquals("No se puede ingresar una cantidad negativa", e.getMessage());
	    }
	}

	@Test
	@DisplayName("Test Ingresar Cero")
	public void testIngresarCero2() {
	    Cuenta cuenta1 = new Cuenta("0002", "María López");
	    try {
	        cuenta1.ingresar("Ingreso en efectivo", 0);
	        fail("Se esperaba una excepción");
	    } catch (Exception e) {
	        assertEquals("No se puede ingresar una cantidad negativa", e.getMessage());
	    }
	}

	@Test
	@DisplayName("Test Retirar Negativo")
	public void testRetirarNegativo2() {
	    Cuenta cuenta1 = new Cuenta("0002", "María López");
	    try {
	        cuenta1.retirar("Retirada de efectivo", -100);
	        fail("Se esperaba una excepción");
	    } catch (Exception e) {
	        assertEquals("No se puede retirar una cantidad negativa", e.getMessage());
	    }
	}

	@Test
	@DisplayName("Test Retirar Saldo Insuficiente")
	public void testRetirarSaldoInsuficiente() throws Exception {
	    Cuenta cuenta1 = new Cuenta("0002", "María López");
	    cuenta1.ingresar("Ingreso en efectivo", 500);
	    try {
	        cuenta1.retirar("Retirada de efectivo", 700);
	        fail("Se esperaba una excepción");
	    } catch (Exception e) {
	        assertEquals("Saldo insuficiente", e.getMessage());
	    }
	}

	@ParameterizedTest
	@CsvSource({ "100.0,50.0,true", "200.0,300.0,false" })
	@DisplayName("Test retiro con fondos insuficientes")
	public void testRetiroDineroInsuficiente(double importe, double retiro, boolean retirado) throws Exception {
		Cuenta cuenta1 = new Cuenta("0004", "Ana García");
		cuenta1.ingresar(importe);
		retirado = true;
		try {
			cuenta1.retirar(retiro);
			assertFalse(!retirado);
		} catch (Exception e) {
			retirado = false;
			assertFalse(retirado);
		}
	}

}