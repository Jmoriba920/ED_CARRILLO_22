package EntornosDesarollo.Cuenta_Tarjeta_Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DebitoTest {

	@Test
	@DisplayName("Test Crear Tarjeta")
	public void testCrearTarjetaDebito() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyy");
		Date fechaPrueba = formato.parse("30/06/2022");
		Debito tarjetaDebito = new Debito("1", "Cliente Genérico", fechaPrueba);

		assertThat(tarjetaDebito, isA(Debito.class));
	}

	@Test
	@DisplayName("Test Retirar Saldo")
	public void testRetirarSaldoDeCuentaConTarjetaDebito() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyy");
		Date fechaPrueba = formato.parse("30/06/2022");
		Debito tarjetaDebito = new Debito("1", "Cliente Genérico", fechaPrueba);
		Cuenta cuentaDebito = new Cuenta("1", "Usuario Genérico");
		cuentaDebito.ingresar(1000);

		tarjetaDebito.setCuenta(cuentaDebito);

		tarjetaDebito.retirar(200);

		assertThat(tarjetaDebito.getSaldo(), equalTo(800.0));
	}

	@Test
	@DisplayName("Test Ingresar Dinero")
	public void testIngresar() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaPrueba = formato.parse("30/06/2022");
		Tarjeta t1 = new Debito("1", "Cliente Genérico", fechaPrueba);
		Cuenta cuenta1 = new Cuenta("1", "Usuario Genérico");
		cuenta1.ingresar(2000);
		t1.setCuenta(cuenta1);
		t1.ingresar(500);
		assertThat(t1.getSaldo(), equalTo(1500.0));
	}

	@Test
	@DisplayName("Test Pago en Establecimiento")
	public void testPagoEnEstablecimiento() throws Exception {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaPrueba = formato.parse("30/06/2022");
		Tarjeta t1 = new Debito("1", "Cliente Genérico", fechaPrueba);
		Cuenta cuenta1 = new Cuenta("1", "Usuario Genérico");

		cuenta1.ingresar(5000);

		t1.setCuenta(cuenta1);

		t1.pagoEnEstablecimiento("Supermercado La Plaza", 85.0);

		assertThat(t1.getSaldo(), equalTo(4915.0));
	}

	@Test
	@DisplayName("Test Get Saldo")
	public void testGetSaldo() throws Exception {
	    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	    Date fechaPrueba = formato.parse("30/06/2022");

	    Tarjeta t1 = new Credito("1", "Usuario Genérico", fechaPrueba, 1500.0);
	    Cuenta cuenta1 = new Cuenta("1", "Usuario Genérico");

	    t1.mCuentaAsociada = cuenta1;

	    t1.retirar(1000.0);

	    assertEquals(50.0, t1.getSaldo());
	}

}