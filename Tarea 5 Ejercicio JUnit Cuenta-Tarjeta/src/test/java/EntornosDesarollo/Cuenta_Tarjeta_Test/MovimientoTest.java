package EntornosDesarollo.Cuenta_Tarjeta_Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class MovimientoTest {

	@Test
	@DisplayName("Test Get Importe")
	public void testGetImporte() {
		Movimiento movimiento = new Movimiento();
		movimiento.setImporte(100.0);
		assertThat(movimiento.getImporte(), is(100.0));
	}

	@Test
	@DisplayName("Test Get Concepto")
	public void testGetConcepto() {
		Movimiento movimiento = new Movimiento();
		movimiento.setConcepto("Pago de factura");
		assertThat(movimiento.getConcepto(), is("Pago de factura"));
	}

	@Test
	@DisplayName("Test Get Fecha")
	public void testGetFecha() {
		Movimiento movimiento = new Movimiento();
		Date fecha = new Date();
		movimiento.setFecha(fecha);
		assertThat(movimiento.getFecha(), is(fecha));
	}

	@Test
	@DisplayName("Test Set Concepto")
	public void testSetConcepto() {
		Movimiento movimiento = new Movimiento();
		movimiento.setConcepto("Pago de factura");
		assertThat(movimiento.getConcepto(), is("Pago de factura"));

		movimiento.setConcepto("Compra en tienda");
		assertThat(movimiento.getConcepto(), is("Compra en tienda"));
	}

	@Test
	@DisplayName("Test Set Fecha")
	public void testSetFecha() {
		Movimiento movimiento = new Movimiento();
		Date fecha1 = new Date();
		movimiento.setFecha(fecha1);
		assertThat(movimiento.getFecha(), is(fecha1));

		Date fecha2 = new Date(1234567890);
		movimiento.setFecha(fecha2);
		assertThat(movimiento.getFecha(), is(fecha2));
	}

	@ParameterizedTest
	@CsvSource({ "100.0,100.0", "200.0,200.0", "0.0,0.0" })
	@DisplayName("Test Set Importe")
	public void testSetImporte(double importe, double expected) {
		Movimiento movimiento = new Movimiento();
		movimiento.setImporte(importe);
		assertThat(movimiento.getImporte(), is(expected));
	}
}
