package EntornosDesarrollo.Test_03_2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class VideojuegoTest {
	Videojuego vj = new Videojuego();
	Videojuego vj1 = new Videojuego("Titulo1", "Compania1");
    Videojuego vj2 = new Videojuego("Titulo1", "Compania1");
    Videojuego vj3 = new Videojuego("Titulo2", "Compania2");
	
	@Test
	@DisplayName("Test de SetTitulo con AssertEquals")
	public void testGetTitulo() {
		vj.setTitulo("Videojuego de prueba");
		assertEquals("Videojuego de prueba", vj.getTitulo());
	}
	
	@Test
	@DisplayName("Test de SetHorasEstimadas con AssertEquals")
	public void testSetHorasEstimadas() {
		vj.setHorasEstimadas(200);
		assertEquals(200, vj.getHorasEstimadas());
	}

	@Test
	@DisplayName("Test de SetGenero con AssertEquals")
	public void testSetGenero() {
		vj.setGenero("Acci�n");
		assertEquals("Acci�n", vj.getGenero());
	}

	@Test
	@DisplayName("Test del m�todo entrega con AssertTrue al m�todo isEntregado")
	public void testEntrega() {
		vj.entrega();
		assertTrue(vj.isEntregado());
	}

	@Test
	@DisplayName("Test del m�todo devolver con AssertFalse al m�todo isEntregado")
	public void testDevolver() {
		vj.devolver();
		assertFalse(vj.isEntregado());
	}

	@Test
	@DisplayName("Test del m�todo compareTo con AssertEquals horas estimadas Videojuego1 es menor que Videojuego2")
	public void testCompareToMenor() {
		vj1.setHorasEstimadas(50);
		vj2.setHorasEstimadas(100);
		assertEquals(-1, vj1.compareTo(vj2));
	}
	
	@Test
	@DisplayName("Test del m�todo compareTo con AssertEquals horas estimadas Videojuego1 es igual que Videojuego2")
	public void testCompareToIgual() {
		vj1.setHorasEstimadas(100);
		vj2.setHorasEstimadas(100);
		assertEquals(0, vj1.compareTo(vj2));
	}
	
	@Test
	@DisplayName("Test del m�todo compareTo con AssertEquals horas estimadas Videojuego1 es mayor que Videojuego2")
	public void testCompareToMayor() {
		vj1.setHorasEstimadas(100);
		vj2.setHorasEstimadas(50);
		assertEquals(1, vj1.compareTo(vj2));
	}
	
	@Test
	@DisplayName("Test del m�todo equals con AssertTrue/AssertFalse al m�todo")
    public void testEquals() {
        assertTrue(vj1.equals(vj2));
        assertFalse(vj1.equals(vj3));
    }
}
