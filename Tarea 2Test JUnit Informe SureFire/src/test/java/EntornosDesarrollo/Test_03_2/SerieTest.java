package EntornosDesarrollo.Test_03_2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SerieTest {
	Serie serie1 = new Serie();
	Serie serie2 = new Serie();

	@Test
	@DisplayName("Test del setTitulo con AssertEquals")
	void testSetTitulo() {
		serie1.setTitulo("Serie 1");
		assertEquals("Serie 1", serie1.getTitulo());
	}

	@Test
	@DisplayName("Test del setnumeroTemporadas con AssertEquals")
	void testSetnumeroTemporadas() {
		serie1.setnumeroTemporadas(5);
		assertEquals(5, serie1.getnumeroTemporadas());
	}

	@Test
	@DisplayName("Test del setGenero con AssertEquals")
	void testSetGenero() {
		serie1.setGenero("Drama");
		assertEquals("Drama", serie1.getGenero());
	}

	@Test
	@DisplayName("Test del setcreador con AssertEquals")
	void testSetcreador() {
		serie1.setcreador("Creador 1");
		assertEquals("Creador 1", serie1.getcreador());
	}

	@Test
	@DisplayName("Test del m�todo entregar con AssertTrue al m�todo isEntregado")
	void testEntregar() {
		serie1.entregar();
		assertTrue(serie1.isEntregado());
	}

	@Test
	@DisplayName("Test del m�todo devolver con AssertFalse al m�todo isEntregado")
	void testDevolver() {
		serie1.devolver();
		assertFalse(serie1.isEntregado());
	}

	@Test
	@DisplayName("Test del m�todo equals con AssertEquals que las temporada de la Serie 1 es menor a Seri 2")
	void testCompareToMenor() {
		serie1.setTitulo("Serie 1");
		serie1.setnumeroTemporadas(3);
		serie2.setTitulo("Serie 2");
		serie2.setnumeroTemporadas(5);
		assertEquals(Serie.MENOR, serie1.compareTo(serie2));
	}
	
	@Test
	@DisplayName("Test del m�todo equals con AssertEquals que las temporada de la Serie 1 es igual a Serie 2")
	void testCompareToIgual() {
		serie1.setTitulo("Serie 1");
		serie1.setnumeroTemporadas(3);
		serie2.setTitulo("Serie 2");
		serie2.setnumeroTemporadas(3);
		assertEquals(Serie.IGUAL, serie1.compareTo(serie2));
	}
	
	@Test
	@DisplayName("Test del m�todo equals con AssertEquals que las temporada de la Serie 1 es mayor a Serie 2")
	void testCompareToMayor() {
		serie1.setTitulo("Serie 1");
		serie1.setnumeroTemporadas(5);
		serie2.setTitulo("Serie 2");
		serie2.setnumeroTemporadas(3);
		assertEquals(Serie.MAYOR, serie1.compareTo(serie2));
	}

}
