package EntornosDesarrollo.Hamcrest;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SerieTest {
	
	@Test
	@DisplayName("Comprueba si se obtiene el título correcto después de establecerlo.")
	public void getTituloTest() {
		Serie serie = new Serie();
		serie.setTitulo("Titulo");
		assertThat(serie.getTitulo(), equalTo("Titulo"));
	}
	
	@Test
	@DisplayName("Comprueba si se establece el título correcto y si no es igual a otro título diferente.")
	public void setTituloTest() {
		Serie serie = new Serie();
		serie.setTitulo("Titulo");
		assertThat(serie.getTitulo(), not(equalTo("Otro titulo")));
	}

	@Test
	@DisplayName("Comprueba si se obtiene el número correcto de temporadas después de establecerlo.")
	public void getnumeroTemporadasTest() {
		Serie serie = new Serie();
		serie.setnumeroTemporadas(5);
		assertThat(serie.getnumeroTemporadas(), equalTo(5));
	}

	@Test
	@DisplayName("Comprueba si se establece el número correcto de temporadas y si no es igual a otro número diferente.")
	public void setnumeroTemporadasTest() {
		Serie serie = new Serie();
		serie.setnumeroTemporadas(5);
		assertThat(serie.getnumeroTemporadas(), not(equalTo(10)));
	}

	@Test
	@DisplayName("Comprueba si se obtiene el género correcto después de establecerlo.")
	public void getGeneroTest() {
		Serie serie = new Serie();
		serie.setGenero("Drama");
		assertThat(serie.getGenero(), equalTo("Drama"));
	}
	
	@Test
	@DisplayName("Comprueba si se establece el género correcto y si no es igual a otro género diferente.")
	public void setGeneroTest() {
		Serie serie = new Serie();
		serie.setGenero("Drama");
		assertThat(serie.getGenero(), not(equalTo("Comedia")));
	}
	
	@Test
	@DisplayName("Comprueba si se obtiene el creador correcto después de establecerlo.")
	public void getcreadorTest() {
		Serie serie = new Serie();
		serie.setcreador("Creador");
		assertThat(serie.getcreador(), equalTo("Creador"));
	}

	@Test
	@DisplayName("Comprueba si se establece el creador correcto y si no es igual a otro creador diferente.")
	public void setcreadorTest() {
		Serie serie = new Serie();
		serie.setcreador("Creador");
		assertThat(serie.getcreador(), not(equalTo("Otro creador")));
	}

	@Test
	@DisplayName("Comprueba si la serie se entrega correctamente.")
	public void entregarTest() {
		Serie serie = new Serie();
		serie.entregar();
		assertThat(serie.isEntregado(), is(true));
	}

	@Test
	@DisplayName("Comprueba si la serie se devuelve correctamente después de ser entregada.")
	public void devolverTest() {
		Serie serie = new Serie();
		serie.entregar();
		serie.devolver();
		assertThat(serie.isEntregado(), is(false));
	}

	@Test
	@DisplayName("Comprueba si la serie se encuentra entregada o no.")
	public void testIsEntregado() {
		Serie serie = new Serie();
		assertThat(serie.isEntregado(), is(not(true)));

		serie.entregar();
		assertThat(serie.isEntregado(), is(true));
	}

	@Test
	@DisplayName("Comprueba si el método compareTo está funcionando correctamente y si devuelve los valores correctos según la comparación.")
	public void testCompareTo() {
		Serie serie1 = new Serie();
		Serie serie2 = new Serie();

		serie1.setnumeroTemporadas(3);
		serie2.setnumeroTemporadas(5);

		assertThat(serie1.compareTo(serie2), is(equalTo(Serie.MENOR)));
		assertThat(serie2.compareTo(serie1), is(equalTo(Serie.MAYOR)));
		assertThat(serie1.compareTo(serie1), is(equalTo(Serie.IGUAL)));
	}

	@Test
	@DisplayName("Comprueba si los valores se establecen correctamente en el constructor y si los atributos son no nulos y no igual a cero.")
	public void testConstructor() {
		Serie serie = new Serie("Titulo", 3, "Genero", "Creador");
		assertThat(serie.getTitulo(), is(notNullValue()));
		assertThat(serie.getcreador(), is(notNullValue()));
		assertThat(serie.getGenero(), is(notNullValue()));
		assertThat(serie.getnumeroTemporadas(), is(not(0)));
		assertThat(serie.isEntregado(), is(false));
	}

}
