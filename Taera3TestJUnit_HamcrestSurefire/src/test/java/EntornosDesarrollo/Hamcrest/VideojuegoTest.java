package EntornosDesarrollo.Hamcrest;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class VideojuegoTest {

	@Test
	@DisplayName("Verifica que el título se pueda establecer y obtener correctamente.")
	public void testTitulo() {
		Videojuego v = new Videojuego();
		v.setTitulo("Titulo prueba");
		assertThat(v.getTitulo(), is("Titulo prueba"));
	}

	@Test
	@DisplayName("Verifica que las horas estimadas se puedan establecer y obtener correctamente.")
	public void testHorasEstimadas() {
		Videojuego v = new Videojuego();
		v.setHorasEstimadas(10);
		assertThat(v.getHorasEstimadas(), is(10));
	}

	@Test
	@DisplayName("Verifica que el género se pueda establecer y obtener correctamente.")
	public void testGenero() {
		Videojuego v = new Videojuego();
		v.setGenero("Acción");
		assertThat(v.getGenero(), is("Acción"));
	}

	@Test
	@DisplayName("Verifica que la compañía se pueda establecer y obtener correctamente.")
	public void testCompañia() {
		Videojuego v = new Videojuego();
		v.setcompañia("Compañia prueba");
		assertThat(v.getcompañia(), is("Compañia prueba"));
	}

	@Test
	@DisplayName("Verifica que el método entregar cambie el estado de entregado a true y el método devolver cambie el estado de entregado a false.")
	public void testEntregado() {
		Videojuego v = new Videojuego();
		v.entregar();
		assertThat(v.isEntregado(), is(true));
		v.devolver();
		assertThat(v.isEntregado(), is(false));
	}

	@Test
	@DisplayName("Verifica el funcionamiento de la implementación del método compareTo, que compara dos objetos Videojuego en base a sus horas estimadas.")
	public void testCompareTo() {
		Videojuego v1 = new Videojuego();
		v1.setHorasEstimadas(10);
		Videojuego v2 = new Videojuego();
		v2.setHorasEstimadas(20);
		assertThat(v1.compareTo(v2), is(Videojuego.MENOR));
		v2.setHorasEstimadas(10);
		assertThat(v1.compareTo(v2), is(Videojuego.IGUAL));
		v2.setHorasEstimadas(5);
		assertThat(v1.compareTo(v2), is(Videojuego.MAYOR));
	}

	@Test
	@DisplayName("Verifica que el constructor Videojuego establezca correctamente los valores de título y género.")
	public void testConstructorVideojuego() {
		Videojuego videojuego = new Videojuego("Titulo", "Genero");
		assertThat(videojuego.getTitulo(), is(notNullValue()));
		assertThat(videojuego.getGenero(), is(notNullValue()));
	}

}
