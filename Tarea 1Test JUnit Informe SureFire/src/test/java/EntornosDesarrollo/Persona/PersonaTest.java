package EntornosDesarrollo.Persona;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class PersonaTest {

	Persona personaTestMenorEdad = new Persona("Jesus", 17, 'H', 76, 1.78);
	Persona personaTest = new Persona("Mario", 18, 'H', 82, 1.85);
	Persona personaTestMayorEdad = new Persona("Maria", 19, 'M', 65, 1.76);

	/**
	 * Create the test case
	 *
	 * @param testPersona
	 */

	/**
	 * Test para comprobar el metodo esMayorDeEdad
	 */
	@Test
	public void testMenorEdad() {
		assertTrue(!personaTestMenorEdad.esMayorDeEdad());
	}

	@Test
	public void testMayorEdadIgual() {
		assertTrue(personaTest.esMayorDeEdad());
	}

	@Test
	public void testMayorEdad() {
		assertTrue(personaTestMayorEdad.esMayorDeEdad());
	}

	/**
	 * Test para comprobar que esa persona tiene InfraPeso
	 */
	@Test
	public void testCalcularIMCInfrapeso() {
		Persona persona = new Persona("John Doe", 25, 'H', 50, 1.8);
		int resultadoEsperado = Persona.INFRAPESO;
		int resultadoObtenido = persona.calcularIMC();
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

	/**
	 * Test para comprobar que esa persona tiene un PesoIdeal
	 */
	@Test
	public void testCalcularIMCPesoIdeal() {
		Persona persona = new Persona("Jane Doe", 22, 'M', 60, 1.7);
		int resultadoEsperado = Persona.PESO_IDEAL;
		int resultadoObtenido = persona.calcularIMC();
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

	/**
	 * Test para comprobar que esa persona tiene Sobrepeso
	 */
	@Test
	public void testCalcularIMCSobrepeso() {
		Persona persona = new Persona("Mike Smith", 30, 'H', 110, 1.8);
		int resultadoEsperado = Persona.SOBREPESO;
		int resultadoObtenido = persona.calcularIMC();
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

}
