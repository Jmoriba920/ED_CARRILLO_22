package EntornosDesarrollo.Test_Shopping_Cart;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.DisplayName;

public class ShoppingCartTest {

	@Test
	@DisplayName("Verifica que un nuevo ShoppingCart tenga 0 elementos y un saldo de 0.0")
	public void testEmptyShoppingCart() {
		ShoppingCart cart = new ShoppingCart();
		assertEquals(0, cart.getItemCount());
		assertEquals(0.0, cart.getBalance(), 0.0);
	}

	@Test
	@DisplayName("Verifica que se pueda agregar un producto al ShoppingCart, aumentando el número de elementos y el saldo correspondientemente.")
	public void testAddProduct() throws ProductNotFoundException {
		ShoppingCart cart = new ShoppingCart();
		Product product = new Product("Yogur", 10.0);
		cart.addItem(product);
		assertEquals(1, cart.getItemCount());
		assertEquals(10.0, cart.getBalance(), 0.0);
	}

	@Test
	@DisplayName("Verifica que se pueda quitar un producto del ShoppingCart, disminuyendo el número de elementos y el saldo correspondientemente.")
	public void testRemoveProduct() throws ProductNotFoundException {
		ShoppingCart cart = new ShoppingCart();
		Product product = new Product("Yogur", 10.0);
		cart.addItem(product);
		cart.removeItem(product);
		assertEquals(0, cart.getItemCount());
		assertEquals(0.0, cart.getBalance(), 0.0);
	}

	@Test
	@DisplayName("Verifica que se lance una excepción al intentar eliminar un producto inexistente del ShoppingCart.")
	public void testRemoveNonexistentProduct() throws ProductNotFoundException {
		ShoppingCart cart = new ShoppingCart();
		Product product = new Product("Yogur", 10.0);
		try {
			cart.removeItem(product);
			fail();
		} catch (ProductNotFoundException e) {
		}
	}
	
	@Test
	@DisplayName("Verifica que se pueda vaciar un ShoppingCart que tenía un producto agregado, dando como resultado 0 elementos y un saldo de 0.0.")
	public void testEmptyShoppingCartAfterAdd() {
		ShoppingCart cart = new ShoppingCart();
		Product product = new Product("Yogur", 10.0);
		cart.addItem(product);
		cart.empty();
		assertEquals(0, cart.getItemCount());
	}
	
	@Test
	@DisplayName("Verifica que un nuevo ShoppingCart tenga 0 elementos y un saldo de 0.0 utilizando los matchers de Hamcrest.")
    public void testEmptyShoppingCartH() {
        ShoppingCart cart = new ShoppingCart();
        assertThat(cart.getItemCount(), is(equalTo(0)));
        assertThat(cart.getBalance(), is(closeTo(0.0, 0.001)));
    }

    @Test
    @DisplayName("Verifica que se pueda agregar un producto al ShoppingCart, aumentando el número de elementos y el saldo correspondientemente utilizando los matchers de Hamcrest.")
    public void testAddProductH() throws ProductNotFoundException {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product("Yogur", 10.0);
        cart.addItem(product);
        assertThat(cart.getItemCount(), is(equalTo(1)));
        assertThat(cart.getBalance(), is(closeTo(10.0, 0.001)));
    }

    @Test
    @DisplayName("Verifica que se pueda quitar un producto del ShoppingCart, disminuyendo el número de elementos y el saldo correspondientemente utilizando los matchers de Hamcrest.")
    public void testRemoveProductH() throws ProductNotFoundException {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product("Yogur", 10.0);
        cart.addItem(product);
        cart.removeItem(product);
        assertThat(cart.getItemCount(), is(equalTo(0)));
        assertThat(cart.getBalance(), is(closeTo(0.0, 0.001)));
    }

    @Test
    @DisplayName("Verifica que se lance una excepción al intentar eliminar un producto inexistente del ShoppingCart utilizando los matchers de Hamcrest.")
    public void testRemoveNonexistentProductH() throws ProductNotFoundException {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product("Yogur", 10.0);
        try {
			cart.removeItem(product);
			fail();
		} catch (ProductNotFoundException e) {
		}
    }

    @Test
    @DisplayName("Verifica que se pueda vaciar un ShoppingCart que tenía un producto agregado, dando como resultado 0 elementos y un saldo de 0.0 utilizando los matchers de Hamcrest.")
    public void testEmptyShoppingCartAfterAddH() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product("Yogur", 10.0);
        cart.addItem(product);
        cart.empty();
        assertThat(cart.getItemCount(), is(equalTo(0)));
    }
}